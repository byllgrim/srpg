# srpg

Simple Role Playing Game

NB! WORK IN PROGRESS

## Goals

Main goal: Make an rpg similar to D&D, Pathfinder, Cogent, BFRPG, etc

Distinction: Be simple, minimalistic, intuitive, fast, adhd

Requirements:
* Intuitive
  * Total newb can create character in ~15 min
  * Combat/Confrontation system fit in human working memory
  * Generic challenge levels
    * (Risus' 5,10,15 as easy,medium,hard?)
    * (Pocket Fantasy RPG's 1->6 as Laughable->VeryDifficult?)
* Flexible
  * Game mechanics totally decoupled from world setting
  * Dice arbitrate outcomes (obviously!)
    * (Prefer ubiquitous d6?)
    * (Challenge level like Cogent?)
    * (Challenge level like Risus?)
* Minimal
  * Fit comfortably on one sheet (two pages) A4 paper
    * (Is it ok to append tables?)
  * Character attributes be orthogonal and few!
    * (Maybe like Cogent's three attributes)
    * (Maybe words related to: Hands, Body, Mind)
    * (Like maple story?)
    * (Maybe omit having classes?)
  * Ability bonus/penalty be severely uncomplicated
    * (Could be equal to the characters attribute score?)
  * Leveling up should follow a constant pattern, not requiring specific table
    lookups for each level
    * (Except spells and such?)
    * (Like the metric system compared to imperial)
  * HP is 1d6
    * (Generalize the concept of HP?)
    * (Also, mana or spells per day?)
* Engaging
  * Oponents and challenges should hold aspects secret for the players
    * (e.g. don't know enemies AC)
  * Melee is fucking lethal
  * Don't feel like artificial resolution limits realistic/intended rulings
    * "The challenge lvl should be kinda in between these two"
  * Don't feel like everythings chance and skills don't matter
  * Don't feel like rules are purely improvised
    * "the floor is lava"
* Free
  * Open source license
    * (Preferably cc0?)
  * Free of charge

## Development plan

Suggestion:
* Gather folks, start a game session without rules
* Every word of gameplay introduces a possible game mechanic
  * E.g. "make up your characters name, for a start?", or "maybe roll a d20 to
    punch the fucker in the face?"

Suggestion:
* Whoever is interested shouts their wishes into this readme
* Or shout to me, byllgrim
* Or create an issue on gitlab

Suggestion:
* A strict review process, "where only the toughest rules survive"
* Separation between "decided" rules, and a list of candidates

Suggestion:
* Very agile, we throw rules in and readily chuck 'em out if they are deemed
  to be shit

Suggestion:
* Do a complete taxonomy of RPGs, then rule out and select features

## Taxonomy of RPGs

No... doing this comprehensively would be too taxing.

* World setting
  * TODO
* Character definition
  * Core stats
    * Available
      * STR,DEX,CON,INT,WIS,CHA (Pathfinder, BFRPG, D&D, etc)
      * STR,REF,INT (Cogent)
      * STR,DEX,INT (Maple Story)
      * STR,AGI,INT,WIS,CHA (Pocket Fantasy RPG)
      * Elder scrolls has tons?
      * Invent your own (Risus)
    * Distribution
      * Player decides
      * In the order rolled
    * Magnitude
      * Fixed at 4,3,2,1 (Risus)
      * Best three of 6d6 (Some Pathfinder?)
      * Plain 3d6 (BFRPG)
      * Only two points (Cogent)
      * Not explicitly filled in, suggested by class (Pocket Fantasy RPG)
  * TODO more
* Gameplay interactions
  * TODO

## Tentative draft

* Game setting
  * GM make up a world and shit
  * Agree upon (or GM decide) 3 setting-dependent character attributes
    * (E.g. STR, DEX, INT for typical fantasy setting)
* Character creation
  * Distribute 2 points onto the available character attributes
    * This defines your class: Two points in STR is maybe a warrior-like type,
      one STR and one INT might be cleric-like
  * Invent your characters name and backstory
  * TODO equipment, gold, spells/specialshit?
* Acting
  1. Make GM accede to what attribute(s?) you'd like to support your action
    * (Maybe they accept none: commoners attempt, still doable)
  2. GM decide challenge level: 5 common, 10 trained, 15 professional
    * (Meets it, beats it!)
  3. Actions unsupported by skills is 1d6, each attribute point adds 1d6
    * (E.g. persuasion with 2 points INT: Roll 3d6)
  4 TODO Re-attempts add 1 to challenge level for each attempt?
* Conflicts
  1. Roll initiative
    * TODO adding modifiers?
  2. Highest numbers act first (equal numbers act simultaneously)
    * TODO damage, armor, etc
  3. Next round, roll initiative again, etc
